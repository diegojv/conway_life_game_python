#!/usr/bin/env python3
import argparse
from random import randint
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
from numba import njit,prange,threading_layer,config
import time


config.THREADING_LAYER = 'omp'

def loadWorldFromFile(filename):
    matrix = np.loadtxt(filename, dtype=int)
    return matrix

def writeWorldToFile(world):
    np.savetxt("result.txt",world,fmt='%i',delimiter="\t",newline="\t\n")

def animate_game(frames):
    """ Receives a set of frames and shows an animation with those images """
    
    # get the figure
    fig = plt.figure()

    # create images from frames
    ims = []
    for elem in frames:
        ims.append((plt.pcolor(elem),))

    # create animation
    im_ani = animation.ArtistAnimation(fig, ims, interval=250, blit=False, repeat=True)
    
    # show the animation
    plt.show()

def createWorld(dimension):
    return np.random.randint(2,size=(dimension,dimension),dtype=int)


@njit(nogil=True)
def getNeighborValue(world,row,column,dimension):
    if(row<0 or row>dimension-1 or column < 0 or column > dimension-1):
        return 0
    else:
        return world[row][column]

@njit(fastmath=True, nogil=True)
def sumNeighbors(world,row,column,dimension):
    result = (getNeighborValue(world, row-1, column-1,dimension) + getNeighborValue(world, row-1, column, dimension)
                 + getNeighborValue(world, row-1, column+1,dimension) + getNeighborValue(world, row, column+1,dimension) 
                 + getNeighborValue(world, row+1, column+1,dimension) + getNeighborValue(world, row+1,column,dimension) 
                 + getNeighborValue(world, row+1, column-1,dimension) +
                 getNeighborValue(world, row,column-1,dimension))

    return result

@njit(parallel=True, nogil=True,fastmath=True)
def computeNextGeneration(world, dimension):
    temporaryWorld = world.copy()
    for row in prange(dimension):
        for column in range(dimension):
            state = world[row][column]
            result = sumNeighbors(world, row, column, dimension)

            if(result<2):
                temporaryWorld[row][column] = 0
            elif(state==1 and (result==2 or result == 3)):
                temporaryWorld[row][column] = state
            elif(result>3):
                temporaryWorld[row][column] = 0
            elif(state==0 and result==3):
                temporaryWorld[row][column] = 1
            else:
                temporaryWorld[row][column] = state
                
    return temporaryWorld            


def conwayLifeGame(world, dimension, generations):
    frames = []
    for iteration in range(generations):
        world = computeNextGeneration(world,dimension)
        frames.append(world)

    return world, frames

def main():
    print("Running Conway's Life Game")
    parser = argparse.ArgumentParser()
    parser.add_argument("dimension", help="Dimension of world (DimensionxDimension)", type=int)
    parser.add_argument("generations", help="Number of generations to simulate", type=int)
    parser.add_argument('-i', '--inputfile', nargs='?', const='arg_was_not_given', help='output file, in JSON format')
    args = parser.parse_args()

    print("Dimension is {}".format(args.dimension))
    print("Running for {} generations".format(args.generations))

    dimension = args.dimension
    generations = args.generations 


    if args.inputfile is None:
        world = createWorld(dimension)
    else:
        world = loadWorldFromFile(args.inputfile)


    start = time.time()
    world, frames = conwayLifeGame(world,dimension,generations)
    end = time.time()
    print("Elapsed time = %s" % (end - start))
    # demonstrate the threading layer chosen
    print("Threading layer chosen: %s" % threading_layer())
    writeWorldToFile(world) 
    #animate_game(frames)


if __name__ == "__main__":
    main()
