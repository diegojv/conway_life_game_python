import argparse
from random import randint
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
import time

def loadWorldFromFile(filename):
    matrix = np.loadtxt(filename, dtype=int)
    return matrix

def writeWorldToFile(world):
    np.savetxt("result.txt",world,fmt='%i',delimiter="\t",newline="\t\n")

def animate_game(frames):
    """ Receives a set of frames and shows an animation with those images """
    
    # get the figure
    fig = plt.figure()

    # create images from frames
    ims = []
    for elem in frames:
        ims.append((plt.pcolor(elem),))

    # create animation
    im_ani = animation.ArtistAnimation(fig, ims, interval=250, blit=False, repeat=True)
    
    # show the animation
    plt.show()

def createWorld(dimension):
    return np.random.randint(2,size=(dimension,dimension),dtype=int)


def computeNextGenerationVectorized(world,dimension):
    Z = world.copy()
    Z = np.pad(Z, pad_width=1,mode='constant', constant_values=0)
    M = np.zeros(Z.shape)

    N = (Z[0:-2, 0:-2] + Z[0:-2, 1:-1] + Z[0:-2, 2:] + Z[1:-1, 0:-2]+ Z[1:-1, 2:] + Z[2: , 0:-2] + Z[2:  , 1:-1] + Z[2:  , 2:])
    birth = (N == 3) & (Z[1:-1, 1:-1] == 0)
    survive = ((N == 2) | (N == 3)) & (Z[1:-1, 1:-1] == 1)
    Z[...] = 0 
    Z[1:-1, 1:-1][birth | survive] = 1 
    M[...] = Z
    return M[1:-1,1:-1]


def conwayLifeGame(world, dimension, generations):
    frames = []
    for iteration in range(generations):
        world = computeNextGenerationVectorized(world,dimension)
        frames.append(world)

    return world, frames

def main():
    print("Running Conway's Life Game")
    parser = argparse.ArgumentParser()
    parser.add_argument("dimension", help="Dimension of world (DimensionxDimension)", type=int)
    parser.add_argument("generations", help="Number of generations to simulate", type=int)
    parser.add_argument('-i', '--inputfile', nargs='?', const='arg_was_not_given', help='output file, in JSON format')
    args = parser.parse_args()

    print("Dimension is {}".format(args.dimension))
    print("Running for {} generations".format(args.generations))

    dimension = args.dimension
    generations = args.generations 


    if args.inputfile is None:
        world = createWorld(dimension)
    else:
        world = loadWorldFromFile(args.inputfile)

    start = time.time()
    world, frames = conwayLifeGame(world,dimension,generations)
    end = time.time()
    print("Elapsed time = %s" % (end - start))
    writeWorldToFile(world) 
    #animate_game(frames)


if __name__ == "__main__":
    main()
