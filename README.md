# conway_life_game_python



## Getting started

This repository holds a Python implementation of Conway's Life Game.

## Execution

There are two execution modes:

1. Random world initialization using just dimension and number of generations
2. Initial input scenario 

```
#Mode 1
python conway dimension generations

#Mode 2
python conway dimension generations -i input_file.txt
```
## Authors and acknowledgment
Developed by Diego Jimenez
Costa Rica National High Technology Center


